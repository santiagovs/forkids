<?php

namespace App\Http\Controllers;

use App\Models\subtema;
use Illuminate\Http\Request;

class SubtemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\subtema  $subtema
     * @return \Illuminate\Http\Response
     */
    public function show(subtema $subtema)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\subtema  $subtema
     * @return \Illuminate\Http\Response
     */
    public function edit(subtema $subtema)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\subtema  $subtema
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subtema $subtema)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\subtema  $subtema
     * @return \Illuminate\Http\Response
     */
    public function destroy(subtema $subtema)
    {
        //
    }
}
