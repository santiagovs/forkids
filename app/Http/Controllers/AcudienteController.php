<?php

namespace App\Http\Controllers;

use App\Models\acudiente;
use Illuminate\Http\Request;

class AcudienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\acudiente  $acudiente
     * @return \Illuminate\Http\Response
     */
    public function show(acudiente $acudiente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\acudiente  $acudiente
     * @return \Illuminate\Http\Response
     */
    public function edit(acudiente $acudiente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\acudiente  $acudiente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, acudiente $acudiente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\acudiente  $acudiente
     * @return \Illuminate\Http\Response
     */
    public function destroy(acudiente $acudiente)
    {
        //
    }
}
