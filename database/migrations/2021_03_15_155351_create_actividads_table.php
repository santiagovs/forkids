<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividads', function (Blueprint $table) {
            $table->integer('idactividad')->autoIncrement();
            $table->string('nombre_actividad',50);
            $table->integer('valor',100);
            $table->string('estado',10);
            $table->string('autor',80);
            $table->integer('idgrado',10);
            $table->integer('iddocente',10);
            $table->integer('idludica',10);
            $table->integer('idtipo_actividad',10);
            $table->string('idusuario',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividads');
    }
}
